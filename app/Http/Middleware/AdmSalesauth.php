<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AdmSalesAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('auth') && session::get('auth')->status != 'admsales') {
            abort(403, 'Unauthorized action.');
        } elseif (!Session::has('auth')) {
            return redirect('/');
        }
        return $next($request);
    }
}
