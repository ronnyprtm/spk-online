<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PrintController extends Controller
{
    public function printsales(Request $req, $id)
    {
        $dataspk = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->leftjoin('spk_karoseri', 'spk_karoseri.spk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('kendaraan.bagian', Session::get('bagian'))
            ->where('no', $id)
            ->first();
        // dd($dataspk);

        $datadeposit = DB::table('deposit')->where('nospk', $id)->get();

        return view('spk.spksales', compact('dataspk', 'id', 'datadeposit')); 
    }
}
