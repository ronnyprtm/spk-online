<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\ImageModel;
use Carbon\Carbon;
use File;
use Image;

class SalesController extends Controller
{
    public function listspk()
    {
        $id = Session::get('id_user');
        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('idsales', $id)
            ->get();

        // dd($data);
        // dd($exist);
        return view('sales.salesspklist', compact('id', 'data'));
    }

    public function home()
    {
        $id = Session::get('id_user');
        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('idsales', $id)
            ->get();

        // dd($data);

        return view('sales.home', compact('id', 'data'));
    }

    public function input($id)
    {
        // dd($id);
        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('no', $id)
            ->first();


        // dd($data);
        // dd($exist);

        //Mengambil Data Kendaraan
        $vehicle = DB::select('SELECT id_kendaraan as id, 
        CONCAT(kendaraan.nama, " ", kendaraan.tipe, " ", kendaraan.warna, " ", kendaraan.tahun) 
        as text from kendaraan where kendaraan.bagian = "'. Session::get('bagian'). '"' );
        // dd($vehicle);

        //Mengambil Data Karoseri

        $karoseri = DB::select('select karoseri.idkaroseri as id, karoseri.namakaroseri as text from karoseri');

        //Mengambil Data Leasing
        $kredit = DB::select('select finance.id_finance as id, finance.nama_finance as text 
                            from finance where finance.jenis = "kredit"');

        //Mengambil Metode Pembayaran Tunai
        $tunai = DB::select('select finance.id_finance as id, finance.nama_finance as text 
                            from finance where finance.jenis = "tunai"');

        //Mengambil Data Harga Mobil
        $price = DB::select('select id_kendaraan, kendaraan.harga from kendaraan');

        // dd($vehicle);
        // dd($price);
        // dd($karoseri);

        return view('sales.form', compact('data', 'vehicle', 'karoseri', 'kredit', 'tunai', 'id'));
    }

    public function insert(Request $req, $id)
    {

        // dd($req->all());
        $id;
        $namacust = $req->namacust;
        $alamatktp = $req->alamat;
        $tujuanbeli = $req->tujuanbeli;
        $nomorktp = $req->nomorktp;
        $telpcust = $req->telpcust;
        $namastnk = $req->namastnk;
        $namafaktur = $req->namafaktur;
        $alamatstnk = $req->alamat;
        $npwpnik = $req->npwpnik;
        $ankontrak = $req->ankontrak;
        $filenpwp = $req->filektp;
        $karoseri = $req->karoseri;
        $noseri = $req->norangka;
        $norangka = $req->norangka;
        $nomesin = $req->nomesin;
        $status = $req->status;
        $ontr = $req->ontr;
        $ontrplat = $req->ontrplat;
        $jenispay = $req->jenispay;
        $warnatnkb = $req->wtnkb;
        $idkendaraan = $req->idkendaraan;
        $idkaroseri = $req->idkaroseri;
        $biayakaroseri = $req->biayakaroseri;
        $sisa = $req->sisa;
        $kvia = $req->krvia;
        $signature = $req->signatureurl;
        $hargaunit = $req->hargaunit;
        $diskon = $req->diskon;
        $dp = $req->dp;
        $filenpwp = $req->filenpwp;
        $filektp = $req->filektp;
        $jkredit = $req->jekredit;
        $idfinance = $req->idfinance;

        $kvia = $req->kvia;
        $ftvia = $req->ftvia;
        $tharga = $req->tharga;
        // dd($status);
        // dd($kvia);


        // dd($signature);
        // dd($req->all());
        // dd($req->image);

        // dd($signature);


        $customerfirst = DB::table('customer')->where('noktp', $nomorktp)->first();

        $tdetailkendaraan = DB::table('detail_kendaraan')->where('nospk', $id)->first();

        // $exist = DB::table('spk')->where('nospk', $id)->first();
        $bagianid = DB::table('kendaraan')->where('id_kendaraan', $idkendaraan)->first();

        $bagian = str_split($bagianid->bagian, 2)[0];

        // dd($bagian);

        $tahun = str_split(Carbon::now()->year, 2)[0];

        $dataterakhirmmksi =  DB::table('spk')->latest('no')->where('no', 'like', 'MM.' . $tahun . '%')->first();
        $dataterakhirmftbc =  DB::table('spk')->latest('no')->where('no', 'like', 'MF.' . $tahun . '%')->first();
        // dd($dataterakhirmftbc);

        // $nomorterakhirmftbc = ++substr($dataterakhirmftbc->no, -4);
        // dd($dataterakhirmmksi->no);
        // dd($dataterakhir);

        if (empty($dataterakhirmmksi) && $bagian == 'MM') {
            $nomorterakhir = 'MM.' . $tahun . '.0001';
        } elseif (empty($dataterakhirmftbc) && $bagian == 'MF') {
            $nomorterakhir = 'MF.' . $tahun . '.0001';
        } elseif (!empty($dataterakhirmfbtc) && $bagian == 'MF') {
            $nomorterakhir = ++$dataterakhirmftbc->no;
        } elseif (!empty($dataterakhirmmksi) && $bagian == 'MM') {
            $nomorterakhir = ++$dataterakhirmmksi->no;            
        }

        // dd($nomorterakhir);
        if ($id == 'input') {
            $folderspk = public_path() . '/spk/' . $nomorterakhir;
        } else {
            $folderspk = public_path() . '/spk/' . $id;
        }

        if (!is_dir($folderspk)) {
            //make new directory with unique id
            mkdir($folderspk, 0777, true);
        }

        $originalpathinsert = public_path() . '/spk/' . $nomorterakhir . '/';
        $thumbnailpathinsert = public_path() . '/spk/' . $nomorterakhir . '/thumbnail/';
        $originalpathid = public_path() . '/spk/' . $id . '/';
        $thumbnailpathid = public_path() . '/spk/' . $id . '/thumbnail/';

        $statusfilektp = $req->hasFile('filektp');
        $statusfilenpwp = $req->hasFile('filenpwp');


        if ($statusfilektp) {
            $imagektp = $req->file('filektp');
            $heightktp = Image::make($imagektp)->height();
            $widthktp = Image::make($imagektp)->width();
            $gambarktp = Image::make($imagektp);

            if ($id == 'input') {
                $namafilektp = 'KTP_' . $nomorterakhir . '.' . $imagektp->getClientOriginalExtension();
                $gambarktp->save($originalpathinsert . $namafilektp);
                $gambarktp->resize($widthktp / 3, $heightktp / 3);
                if (!is_dir($thumbnailpathinsert)) {
                    //make new directory with unique id
                    mkdir($thumbnailpathinsert, 0777, true);
                }
                $gambarktp->save($thumbnailpathinsert . 'KTP_' . $nomorterakhir . '.' . $imagektp->getClientOriginalExtension());
            } else {
                $namafilektp = 'KTP_' . $id . '.' . $imagektp->getClientOriginalExtension();
                $gambarktp->save($originalpathid . $namafilektp);
                $gambarktp->resize($widthktp / 3, $heightktp / 3);
                if (!is_dir($thumbnailpathid)) {
                    //make new directory with unique id
                    mkdir($thumbnailpathid, 0777, true);
                }
                $gambarktp->save($thumbnailpathid . 'KTP_' . $id . '.' . $imagektp->getClientOriginalExtension());
            }
        }

        if ($statusfilenpwp) {
            $imagenpwp = $req->file('filenpwp');
            $heightnpwp = Image::make($imagenpwp)->height();
            $widthnpwp = Image::make($imagenpwp)->width();
            $gambarnpwp = Image::make($imagenpwp);

            if ($id == 'input') {
                $namafilenpwp = 'NPWP_' . $nomorterakhir . '.' . $imagenpwp->getClientOriginalExtension();
                $gambarnpwp->save($originalpathinsert .  $namafilenpwp);
                $gambarnpwp->resize($widthnpwp / 3, $heightnpwp / 3);
                if (!is_dir($thumbnailpathinsert)) {
                    //make new directory with unique id
                    mkdir($thumbnailpathinsert, 0777, true);
                }
                $gambarnpwp->save($thumbnailpathinsert . $namafilenpwp);
            } else {
                $namafilenpwp = 'NPWP_' . $id . '.' . $imagenpwp->getClientOriginalExtension();
                $gambarnpwp->save($originalpathid . $namafilenpwp);
                $gambarnpwp->resize($widthnpwp / 3, $heightnpwp / 3);
                if (!is_dir($thumbnailpathid)) {
                    //make new directory with unique id
                    mkdir($thumbnailpathid, 0777, true);
                }
                $gambarnpwp->save($thumbnailpathid . $namafilenpwp);
            }
        }




        // dd($namafilenpwp);



        // dd($image->getClientOriginalExtension());


        $file_data = $signature;
        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);

        if ($id == 'input') {
            $filepath = public_path() . '/spk/' . $nomorterakhir;
            $file_name = 'signature_' . $nomorterakhir . '.png'; //generating unique file name; 
        } else {
            $filepath = public_path() . '/spk/' . $id;
            $file_name = 'signature_' . $id . '.png'; //generating unique file name; 

        }

        // dd($filepath);

        if (!is_dir($filepath)) {
            //make new directory with unique id
            mkdir($filepath, 0777, true);
        }

        // dd($filepath);
        // dd($nospk);
        file_put_contents($filepath . '/' . $file_name, base64_decode($file_data));


        $exist = DB::table('spk')->where('no', $id)->first();
        // dd($exist);

        // dd($file_name);

        if ($exist) {
            // jika data ada maka update
            $depositdata = $req->all();
            for ($i = 0; $i < count($depositdata['viapay']); $i++) {

                $updatedeposit[] = [
                    'nospk' => $id,
                    'tanggal' => $depositdata['tanggalpay'][$i],
                    'via' => $depositdata['viapay'][$i],
                    'nilai' => $depositdata['nilaipay'][$i],
                    'atasnama' => $depositdata['anpay'][$i]
                ];
            }
            DB::table('deposit')->where('nospk', $id)->delete();
            DB::table('deposit')->insert($updatedeposit);

            $updatecustomer = [
                "noktp" => $nomorktp,
                "npwpnik" => $npwpnik,
                "namacust" => $namacust,
                "alamatktp" => $alamatktp,
                "notelp" => $telpcust,
            ];

            $updatespk = [
                "pemesan" => $nomorktp,
                "anfakturstnk" => $namastnk,
                "anpajak" => $namafaktur,
                "keperluan" => $tujuanbeli,
                "ankontrak" => $ankontrak,
                "idkaroseri" => $idkaroseri,
                "statusplat" => $status,
                "namaplat" => $ontrplat,
                "warnaplat" => $warnatnkb,
                "jenispembayaran" => $jenispay,
                "alamatstnk" => $alamatstnk
                // "sign" => $file_name
            ];

            $insertspkuser = [
                "diskon" => $diskon,
                "dp" => $dp
            ];



            $updatedetail = [
                "detailkendaraanid" => $idkendaraan
            ];

            if ($id == 'input') {
                $insertdetail = [
                    "detailkendaraanid" => $idkendaraan,
                    "nospk" => $nomorterakhir
                ];
            } else {
                $insertdetail = [
                    "detailkendaraanid" => $idkendaraan,
                    "nospk" => $id
                ];
            }


            $updatedeposit = [];

            if (!empty($statusfilektp)) {
                $insertfilektp = $namafilektp;
            } else {
                $insertfilektp = null;
            }

            if (!empty($statusfilenpwp)) {
                $insertfilenpwp = $namafilenpwp;
            } else {
                $insertfilenpwp = null;
            }


            $updatespkcust = [
                'idcustomer' => $nomorktp,

                "filektp" => $insertfilektp,

                "filenpwp" => $insertfilenpwp,

                "sign" => $file_name
            ];

            if ($tdetailkendaraan) {
                DB::table('detail_kendaraan')->where('nospk', $id)->update($updatedetail);
            } else {
                DB::table('detail_kendaraan')->insert($insertdetail);
            }

            if($jenispay == 'kredit' && $id == 'input'){
                $updatekredit = [
                    "no_spk" => $nomorterakhir,
                    "idfinance" => $kvia
                ];
            }

            if ($bagian == 'MF') {
                $updatekaroseri = [
                    'id_karoseri' => $req->idkaroseri,
                    'biaya_karoseri' => $req->biayakaroseri
                ];
                DB::table('spk_karoseri')->where('spk', $id)->update($updatekaroseri);
            }
            DB::table('spk')->where('no', $id)->update($updatespk);
            DB::table('customer')->where('noktp', $nomorktp)->update($updatecustomer);
            DB::table('spk_user')->where('nospk', $id)->update($insertspkuser);
            return redirect('/sales/list/spk')->with('alert', 'Sukses Di Edit!');
        } else {
            $depositdata = $req->all();

            for ($i = 0; $i < count($depositdata['viapay']); $i++) {

                $insertdeposit[] = [
                    'nospk' => $nomorterakhir,
                    'tanggal' => $depositdata['tanggalpay'][$i],
                    'via' => $depositdata['viapay'][$i],
                    'nilai' => $depositdata['nilaipay'][$i],
                    'atasnama' => $depositdata['anpay'][$i]
                ];
            }
            // DB::table('deposit')->where('nospk', $nomorterakhir))->delete();
            DB::table('deposit')->insert($insertdeposit);

            $insertspk = [
                "pemesan" => $nomorktp,
                "anfakturstnk" => $namastnk,
                "keperluan" => $tujuanbeli,
                "anpajak" => $namafaktur,
                "ankontrak" => $ankontrak,
                "idkaroseri" => $idkaroseri,
                "statusplat" => $status,
                "namaplat" => $ontrplat,
                "warnaplat" => $warnatnkb,
                "jenispembayaran" => $jenispay,
                "no" => $nomorterakhir,
                "alamatstnk" => $alamatstnk
            ];

            $insertcustomer = [
                "noktp" => $nomorktp,
                "npwpnik" => $npwpnik,
                "namacust" => $namacust,
                "alamatktp" => $alamatktp,
                "notelp" => $telpcust,
            ];

            $insertspkuser = [
                "nospk" => $nomorterakhir,
                "tanggal" => Carbon::now(),
                "idsales" => Session::get("id_user"),
                "diskon" => $diskon,
                "dp" => $dp,
            ];


            $updatecustomer = [
                "noktp" => $nomorktp,
                "npwpnik" => $npwpnik,
                "namacust" => $namacust,
                "alamatktp" => $alamatktp,
                "notelp" => $telpcust
            ];

            if (!empty($statusfilektp)) {
                $insertfilektp = $namafilektp;
            } else {
                $insertfilektp = null;
            }

            if (!empty($statusfilenpwp)) {
                $insertfilenpwp = $namafilenpwp;
            } else {
                $insertfilenpwp = null;
            }

            $insertspkcust = [
                'nospk' => $nomorterakhir,
                'idcustomer' => $nomorktp,
                "filektp" => $insertfilektp,
                "filenpwp" => $insertfilenpwp,
                "sign" => $file_name
            ];

            $updatedetail = [
                "detailkendaraanid" => $idkendaraan
            ];

            $insertdetail = [
                "detailkendaraanid" => $idkendaraan,
                "nospk" => $nomorterakhir
            ];


            if ($tdetailkendaraan) {
                DB::table('detail_kendaraan')->where('nospk', $id)->update($updatedetail);
            } else {
                DB::table('detail_kendaraan')->insert($insertdetail);
            }
            

            if ($jenispay == 'kredit') {
                $insertkredit = [
                "no_spk" => $nomorterakhir,
                "idfinance" => $idfinance,
                "jeniskredit" => $jkredit
                ];

                DB::table('kredit')->insert($insertkredit);
            }


            if ($bagian == 'MF') {
                $insertkaroseri = [
                    'spk' => $nomorterakhir,
                    'id_karoseri' => $req->idkaroseri,
                    'biaya_karoseri' => $req->biayakaroseri
                ];
                DB::table('spk_karoseri')->insert($insertkaroseri);
            }

            DB::table('spk')->insert($insertspk);
            // dd($customerfirst);

            if ($customerfirst) {
                DB::table('customer')->where('noktp', $nomorktp)->update($updatecustomer);
            } else {
                DB::table('customer')->insert($insertcustomer);
            }

            DB::table('spk_user')->insert($insertspkuser);
            DB::table('spk_customer')->insert($insertspkcust);
        }

        // dd($updatecustomer);
        // // dd($image);

        // file_put_contents($path, base64_decode($base64string));

    }

    public function confirm(Request $req, $id)
    {
        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->leftjoin('spk_karoseri', 'spk_karoseri.spk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('no', $id)
            ->first();

        $datadeposit = DB::table('deposit')->where('nospk', $id)->get();

            // dd($datadeposit);

        $id;
        $namacust = $req->namacust;
        $alamatktp = $req->alamatktp;
        $tujuanbeli = $req->tujuanbeli;
        $nomorktp = $req->nomorktp;
        $telpcust = $req->telpcust;
        $namastnk = $req->namastnk;
        $namafaktur = $req->namafaktur;
        $alamatstnk = $req->alamat;
        $npwpnik = $req->npwpnik;
        $ankontrak = $req->ankontrak;
        $filenpwp = $req->filektp;
        $karoseri = $req->karoseri;
        $noseri = $req->norangka;
        $norangka = $req->norangka;
        $nomesin = $req->nomesin;
        $status = $req->status;
        $ontr = $req->ontr;
        $jenispay = $req->jenispay;
        $warnatnkb = $req->warnatnkb;
        $jkredit = $req->jkredit;
        $kvia = $req->kvia;
        $ftvia = $req->ftvia;
        $tharga = $req->tharga;
        $idkaroseri = $req->idkaroseri;
        // dd(request()->all());
        //   "telpcust" => "00000000000"
        //   "namastnk" => "Ronny Pratama"
        //   "namafaktur" => "Ronny Pratama"
        //   "alamat" => "Jl. Kiri Kanan"
        //   "npwpnik" => "00000"
        //   "ankontrak" => "joko"
        //   "filektp" => "Capture.PNG"
        //   "filenpwp" => "EDIT.PNG"
        //   "kendaraan" => "1"
        //   "karoseri" => "1"
        //   "noseri" => "12"
        //   "norangka" => "122"
        //   "nomesin" => "1322"
        //   "status" => "ontr"
        //   "ontr" => "DA"
        //   "jenispay" => "tunai"
        //   "kvia" => null
        //   "ftvia" => "BCA"
        //   "jkredit" => null
        //   "tharga" => null;
        $kendaraan = $req->kendaraan;
        $tipe = DB::table('kendaraan')->where('id_kendaraan', $kendaraan)->first();
        // dd($tipe);
        $idkaroseri = $req->karoseri;
        if (!empty($idkaroseri)) {
            $karoseri = DB::table('karoseri')->where('idkaroseri', $idkaroseri)->first();
        }

        $datafinance = DB::table('finance')->where('id_finance', $kvia)->first(); 

        // dd($data);

            // dd($tipe->harga + $data->biaya_karoseri - $data->diskon);


        // dd($datafinance);
        // dd($karoseri);
        // $bentuk = DB::select('select kendaraan.tipe from kendaraan where id_kendaraan = ' . $kendaraan)->first();

        return view('sales.konfirmasi', compact(
            'id',
            'namacust',
            'alamatktp',
            'nomorktp',
            'tujuanbeli',
            'telpcust',
            'namafaktur',
            'namastnk',
            'alamatstnk',
            'npwpnik',
            'ankontrak',
            'tipe',
            'kendaraan',
            'idkaroseri',
            'karoseri',
            'status',
            'ontr',
            'jenispay',
            'warnatnkb',
            'data',
            'kvia',
            'jkredit',
            'datafinance',
            'datadeposit',
        ));
    }
}
