<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AdminSalesController extends Controller
{
    public function index()
    {

        // Mengambil Data Kendaraan
        $vehicle = DB::select('SELECT id_kendaraan as id, 
        CONCAT(kendaraan.nama, " ", kendaraan.tipe, " ", kendaraan.warna, " ", kendaraan.tahun) 
        as text from kendaraan');

        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->leftJoin('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('nomor_kendaraan', 'nomor_kendaraan.no_rangka', '=', 'detail_kendaraan.no_rangka')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'spk.idkendaraan')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->join('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->get();

        // dd($data);

        // dd($data);
        $karoseri = DB::select('select karoseri.idkaroseri as id, karoseri.namakaroseri as text from karoseri');

        return view('sales_adm.listspk', compact('vehicle', 'data', 'karoseri'));
    }

    public function dashboard()
    {

        // Mengambil Data Kendaraan
        $vehicle = DB::select('SELECT id_kendaraan as id, 
        CONCAT(kendaraan.nama, " ", kendaraan.tipe, " ", kendaraan.warna, " ", kendaraan.tahun) 
        as text from kendaraan');

        // dd(Session::get('bagian'));

        $data = DB::table('spk')
            ->join('customer', 'customer.noktp', '=', 'spk.pemesan')
            ->Join('detail_kendaraan', 'detail_kendaraan.nospk', '=', 'spk.no')
            ->leftjoin('karoseri', 'karoseri.idkaroseri', '=', 'spk.idkaroseri')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'detail_kendaraan.detailkendaraanid')
            ->join('spk_user', 'spk_user.nospk', '=', 'spk.no')
            ->join('user', 'user.id_user', '=', 'spk_user.idsales')
            ->leftjoin('kredit', 'kredit.no_spk', '=', 'spk.no')
            ->where('kendaraan.bagian', Session::get('bagian'))
            ->where('admsalesverif', '0')
            ->orderBy('spk.no', 'ASC')
            ->get();

        $datadeposit = DB::table('deposit')->get();

        // dd($datadeposit);
        // dd($data);

        // dd($data);
        $karoseri = DB::select('select karoseri.idkaroseri as id, karoseri.namakaroseri as text from karoseri');

        return view('sales_adm.listspk', compact('vehicle', 'data', 'karoseri', 'datadeposit'));
    }

    public function detaildeposit(Request $req, $id)
    {
        $datadeposit = DB::table('deposit')->where('nospk', $id)->get();
        // dd($datadeposit);
        return view('sales_adm.popupdeposit', compact('datadeposit', 'id'));
    }

    public function postdeposit(Request $req, $id)
    {
        // dd($req->all());
        $exist = DB::table('deposit')->where('nospk', $id)->get();

        if ($exist) {
            $depositdata = $req->all();
            for ($i = 0; $i < count($depositdata['viapay']); $i++) {

                $updatedeposit[] = [
                    'nospk' => $id,
                    'tanggal' => $depositdata['tanggalpay'][$i],
                    'via' => $depositdata['viapay'][$i],
                    'nilai' => $depositdata['nilaipay'][$i],
                    'atasnama' => $depositdata['anpay'][$i]
                ];
            }

            DB::table('deposit')->where('nospk', $id)->delete();
            DB::table('deposit')->insert($updatedeposit);
        }else{
            $depositdata = $req->all();
            // dd($depositdata);
            for ($i = 0; $i < count($depositdata['viapay']); $i++) {

                $updatedeposit[] = [
                    'nospk' => $id,
                    'tanggal' => $depositdata['tanggalpay'][$i],
                    'via' => $depositdata['viapay'][$i],
                    'nilai' => $depositdata['nilaipay'][$i],
                    'atasnama' => $depositdata['anpay'][$i]
                ];
            }

            DB::table('deposit')->insert($updatedeposit);
        }
        return Redirect::to('/admsales/');
    }

    public function norangka(Request $req, $id)
    {
        // dd($req->kendaraan);
        $idkendaraan = $req->nokendaraan;
        $no_rangka = $req->no_rangka;
        $no_seri = $req->no_seri;
        $no_mesin = $req->no_mesin;

        $dkexist = DB::table('detail_kendaraan')->where('nospk', $id)->first();

        if ($dkexist) {
            $updatedk = [
                'no_rangka' => $no_rangka,
                'detailkendaraanid' => $idkendaraan,
                'no_rangka' => $no_rangka,
                'no_seri' => $no_seri,
                'no_mesin' => $no_mesin
            ];

            DB::table('detail_kendaraan')->where('nospk', $id)->update($updatedk);
        } else {
            $insertdk = [
                'nospk' => $id,
                'no_rangka' => $no_rangka,
                'detailkendaraanid' => $idkendaraan,
                'no_rangka' => $no_rangka,
                'no_seri' => $no_seri,
                'no_mesin' => $no_mesin
            ];
            DB::table('detail_kendaraan')->insert($insertdk);
        }

    }
}
