<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $req)
    {
        $exists = DB::table('user')
            ->where('id_user', $req->id)
            ->where('password', $req->Password)->first();
        Session::put('id_user', $exists->id_user);
        Session::put('nama_user', $exists->nama_user);
        Session::put('cabang', $exists->cabang);
        Session::put('bagian', $exists->bagian);
        // dd($exists);
        // dd($exists);

        if ($exists) {
            Session::put('auth', $exists);
            if ($exists) {
                redirect('/')->with('cek', 'Sukses Login');
            } else {
                $redi = redirect('/')->with('cek', 'Login Gagal, Silahkan Cek Kembali Password Anda');
            }

            if ($exists->status == 'sales') {
                return redirect('/sales');
            } elseif ($exists->status == 'admsales') {
                return redirect('/admsales');
            } elseif ($exists->status == 'manager') {
                return redirect('/manager');
            } elseif ($exists->status == 'branchmanager') {
                return redirect('/branchmanager');
            }
            else {
                return redirect('/')->with('cek', 'Login Gagal, Silahkan Cek Kembali NIP Dan Password Anda');
            }
        }
    }
    
    public function logout(Request $req)
    {
        Session::forget('auth');
        return redirect('/');
    }
}
