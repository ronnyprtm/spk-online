<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdminSalesController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::get('/', function () {
    return view('login');
});
Route::post('/', 'LoginController@login');
Route::get('/test', function () {
    return view('spk.spk');
});

Route::group(['middleware' => 'SalesAuth'], function () {

Route::get('/sales', 'SalesController@home');

Route::get('/sales/form/{id}', 'SalesController@input');
Route::post('/sales/form/{id}/confirm', 'SalesController@confirm');
Route::post('/sales/form/{id}/insert', 'SalesController@insert');
Route::get('/sales/list/spk', 'SalesController@listspk');
Route::get('/sales/logout', 'LoginController@logout');
});

Route::group(['middleware' => 'AdmSalesAuth'], function () {  
    Route::get('/admsales', 'AdminSalesController@Dashboard');
    Route::get('/admsales/datadeposit/{id}', 'AdminSalesController@detaildeposit');
    Route::post('/admsales/datadeposit/{id}', 'AdminSalesController@postdeposit');

    Route::post('/admsales/{id}/insert/norangka','AdminSalesController@norangka');
    Route::post('/admsales/{id}/insert/detail/spk','AdminSalesController@verifspk');
    Route::get('/admsales/listspk','AdminSalesController@index');
    Route::get('/admsales/logout', 'LoginController@logout');

});

Route::group(['middleware' => 'ManagerAuth'], function () {
    Route::get('/manager', function () {
        return view('manager.home');
    });

    Route::get('/manager/logout', 'LoginController@logout');
});

Route::get('/print/sales/{id}', 'PrintController@printsales');


// Route::get('/listspk', function () {
//     return view('sales_adm.listspk');
// });



