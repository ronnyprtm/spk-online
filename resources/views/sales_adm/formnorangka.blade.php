@extends('sales_adm.layout')
@section('heading')
<h2>Konfirmasi Pengisian Nomor Rangka</h2>
@endsection
@section('content')
<div class="panel">
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="grid-input-1" class="col-md-2 control-label">Nomor Rangka</label>
                <div class="col-md-9">
                    <input type="password" class="form-control" id="norangka" name="norangka"
                        placeholder="Masukkan Nomor Rangka">
                </div>
            </div>
            <div class="form-group">
                <label for="grid-input-1" class="col-md-2 control-label">Nomor Mesin</label>
                <div class="col-md-9">
                    <input type="password" class="form-control" id="nomesin" name="nomesin"
                        placeholder="Masukkan Nomor Mesin">
                </div>
            </div>
            <div class="form-group">
                <label for="grid-input-1" class="col-md-2 control-label">Nomor Seri</label>
                <div class="col-md-9">
                    <input type="password" class="form-control" id="noseri" name="noseri"
                        placeholder="Masukkan Nomor Seri">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-9">
                    <button type="submit" class="btn">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection