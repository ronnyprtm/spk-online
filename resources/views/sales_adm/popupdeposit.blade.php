@extends('sales_adm.layout')
@section('heading')

@endsection
@section('content')
<div class="container">
    <div class="panel">
    <div class="panel-title">Detail Deposit {{$id}}</div>
        <div class="panel-body">
            <form action="/admsales/datadeposit/{{$id}}" method="POST">
                @csrf
            <div class="row">
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder="" value="{{isset($datadeposit[0]->tanggal) ? $datadeposit[0]->tanggal : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Via</label>
                    <input type="text" class="form-control" id="viapay" name="viapay[]"
                        placeholder="Masukkan Via Pembayaran" value="{{isset($datadeposit[0]->via) ? $datadeposit[0]->via : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                    <input type="text" class="form-control" id="nilaipay" name="nilaipay[]"
                        placeholder="Masukkan Nilai Pembayaran" value="{{isset($datadeposit[0]->nilai) ? $datadeposit[0]->nilai : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                    <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama" 
                    value="{{isset($datadeposit[0]->atasnama) ? $datadeposit[0]->atasnama : ''}}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                    <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder=""
                        value="{{isset($datadeposit[1]->tanggal) ? $datadeposit[1]->tanggal : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Via</label>
                    <input type="text" class="form-control" id="viapay" name="viapay[]" placeholder="Masukkan Via Pembayaran"
                        value="{{isset($datadeposit[1]->via) ? $datadeposit[1]->via : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                    <input type="text" class="form-control" id="nilaipay" name="nilaipay[]" placeholder="Masukkan Nilai Pembayaran"
                        value="{{isset($datadeposit[1]->nilai) ? $datadeposit[1]->nilai : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                    <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama"
                        value="{{isset($datadeposit[1]->atasnama) ? $datadeposit[1]->atasnama : ''}}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                    <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder=""
                        value="{{isset($datadeposit[2]->tanggal) ? $datadeposit[2]->tanggal : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Via</label>
                    <input type="text" class="form-control" id="viapay" name="viapay[]" placeholder="Masukkan Via Pembayaran"
                        value="{{isset($datadeposit[2]->via) ? $datadeposit[2]->via : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                    <input type="text" class="form-control" id="nilaipay" name="nilaipay[]" placeholder="Masukkan Nilai Pembayaran"
                        value="{{isset($datadeposit[2]->nilai) ? $datadeposit[2]->nilai : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                    <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama"
                        value="{{isset($datadeposit[2]->atasnama) ? $datadeposit[2]->atasnama : ''}}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                    <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder=""
                        value="{{isset($datadeposit[3]->tanggal) ? $datadeposit[3]->tanggal : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Via</label>
                    <input type="text" class="form-control" id="viapay" name="viapay[]" placeholder="Masukkan Via Pembayaran"
                        value="{{isset($datadeposit[3]->via) ? $datadeposit[3]->via : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                    <input type="text" class="form-control" id="nilaipay" name="nilaipay[]" placeholder="Masukkan Nilai Pembayaran"
                        value="{{isset($datadeposit[3]->nilai) ? $datadeposit[3]->nilai : ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                    <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama"
                        value="{{isset($datadeposit[3]->atasnama) ? $datadeposit[3]->atasnama : ''}}">
                </div>
            </div>
        </div>    
        <button type="submit" class="btn btn-success">Success</button>
        </form>
    </div>

    @endsection