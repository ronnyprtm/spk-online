@extends('sales_adm.layout');
@section('heading')
<h2> List SPK </h2>
@endsection

@section('content')
<div class="panel">
    <div class="panel-body">
        <div class="table-info">
            {{-- <div class="table-header">
                <div class="table-caption">
                    Info Table
                </div>
            </div> --}}
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No. SPK</th>
                            <th>Nama Kendaraan</th>
                            <th>No. Rangka</th>
                            <th>Harga</th>
                            <th>Nama Sales</th>
                            <th>Status</th>
                            <th>Status Pembayaran</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $datas)
                        <tr>
                            <th>{{ $datas->no }}</th>
                            <th>{{ $datas->nama.' '.$datas->tipe.' '.$datas->warna.' '.$datas->tahun }}</th>
                            <th><button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#rangka{{str_replace('.', '', $datas->no)}}">Detail No. Rangka</button>
                            </th>
                            <th>{{ $datas->harga}}</th>
                            <th>{{ $datas->nama_user }}</th>
                            <th><button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#detail{{str_replace('.', '', $datas->no)}}">Detail</button></th>
                            <th><a href="/admsales/datadeposit/{{$datas->no}}" target="_blank"><button type="button" class="btn btn-primary btn-lg" data-toggle="modal">Detail</button></a></th>
                            <th>a</th>
                        </tr>

                        <div class="modal fade" id="detail{{str_replace('.', '', $datas->no)}}" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Data SPK {{$datas->no}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"
                                            action="/admsales/{{$datas->nospk}}/insert/detail/spk" method="POST">
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-1" class="col-md-3 control-label">Nama
                                                    Customer</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="namacust"
                                                        name="namacust" placeholder="Masukkan Nama Customer"
                                                        value="{{ isset($datas->namacust)? $datas->namacust : '' }}">
                                                    <small class="text-muted">Masukkan Nama Customer</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-1" class="col-md-3 control-label">Alamat
                                                    KTP</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="alamatktp"
                                                        name="alamatktp"
                                                        value="{{ isset($datas->alamatktp)? $datas->alamatktp : '' }}">
                                                    <small class="text-muted">Masukkan Alamat KTP</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2"
                                                    class="col-md-3 control-label">Keperluan</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="keperluan"
                                                        name="keperluan" placeholder="Masukkan Keperluan"
                                                        value="{{ isset($datas->keperluan)? $datas->keperluan : '' }}">
                                                    <small class="text-muted">Masukkan Keperluan Pembelian </small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">No.
                                                    Telp/HP</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="notelp" name="notelp"
                                                        placeholder="Masukkan Keperluan"
                                                        value="{{ isset($datas->keperluan)? $datas->keperluan : '' }}">
                                                    <small class="text-muted">Masukkan Keperluan Pembelian </small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Faktur STNK
                                                    a/n.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="anfakturstnk"
                                                        name="anfakturstnk" placeholder="Masukkan Nomor Mesin Kendaraan"
                                                        value="{{ isset($datas->anfakturstnk)? $datas->anfakturstnk : '' }}">
                                                    <small class="text-muted">Masukkan Atas Nama Untuk Faktur
                                                        STNK</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Faktur Pajak
                                                    a/n.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="anpajak" name="anpajak"
                                                        placeholder="Masukkan Atas Nama Faktur Pajak"
                                                        value="{{ isset($datas->anpajak)? $datas->anpajak : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Atas Nama Faktur
                                                        Pajak</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">NPWP /
                                                    NIK</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="npwpnik" name="npwpnik"
                                                        placeholder="Masukkan Atas NPWP/NIK"
                                                        value="{{ isset($datas->npwpnik)? $datas->npwpnik : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan NPWP / NIK</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Kontrak
                                                    a/n.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="ankontrak"
                                                        name="ankontrak" placeholder="Masukkan Atas NPWP/NIK"
                                                        value="{{ isset($datas->ankontrak)? $datas->ankontrak : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan NPWP / NIK</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Tipe /
                                                    Bentuk</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control"
                                                        value="{{ isset($datas->id_kendaraan)? $datas->nama.' / '.$datas->tipe : '' }}"
                                                        readonly>
                                                    <small class="text-muted">Silahkan Masukkan Tipe Kendaraan</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Tahun /
                                                    Warna</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control"
                                                        value="{{ isset($datas->id_kendaraan)? $datas->tahun.' / '.$datas->warna : '' }}"
                                                        readonly>
                                                    <small class="text-muted">Silahkan Masukkan Tipe Kendaraan</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2"
                                                    class="col-md-3 control-label">Karoseri</label>
                                                <div class="col-md-9">
                                                    <select class="custom-select form-control" id="karoseri"
                                                        name="karoseri">
                                                        <option value="">Belum Dipilih</option>
                                                        @foreach ($karoseri as $karo)
                                                        <option value="{{$karo->id}}"
                                                            {{ isset($data->idkaroseri) == $karo->id? 'selected' : '' }}>
                                                            {{$karo->text}}</option>
                                                        @endforeach
                                                    </select>
                                                    <small class="text-muted">Silahkan Masukkan Karoseri</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Plat
                                                    a/n.</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="ankontrak"
                                                        name="ankontrak" placeholder="Masukkan Atas NPWP/NIK"
                                                        value="{{ isset($datas->ankontrak)? $datas->ankontrak : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan NPWP / NIK</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Jenis
                                                    Pembayaran</label>
                                                <div class="col-md-9">
                                                    <select class="custom-select form-control" id="jenispay"
                                                        name="jenispay">
                                                        <option>Belum Dipilih</option>
                                                        <option value="tunai"
                                                            {{isset($datas->jenispembayaran) && $datas->jenispembayaran == 'tunai'? 'selected' : '' }}>
                                                            Tunai</option>
                                                        <option value="kredit"
                                                            {{isset($datas->jenispembayaran) && $datas->jenispembayaran == 'kredit'? 'selected' : '' }}>
                                                            Kredit</option>
                                                    </select>
                                                    <small class="text-muted">Silahkan Masukkan Jenis Pembayaran</small>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Jenis
                                                    Pembayaran Kredit</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="jenispaykredit" name="jenispaykredit"
                                                        placeholder="Masukkan Atas NPWP/NIK" value="{{ isset($datas->jeniskredit)? $datas->jeniskredit : '' }}">
                                            <small class="text-muted">Silahkan Masukkan Jenis Kredit</small>
                                    </div>
                                </div> --}}
                                <div class="form-group form-group-sm">
                                    <label for="grid-input-sm-2" class="col-md-3 control-label">Jenis Kredit</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="jenispaykredit"
                                            name="jenispaykredit"
                                            value="{{ isset($datas->jeniskredit)? $datas->jeniskredit : '' }}" readonly>
                                        <small class="text-muted">Silahkan Masukkan Jenis Kredit</small>
                                    </div>
                                </div>

                                <div class="form-group form-group-sm">
                                    <label for="grid-input-sm-2" class="col-md-3 control-label">Status
                                        Plat</label>
                                    <div class="col-md-9">
                                        <select class="custom-select form-control" id="statusplat" name="statusplat">
                                            <option>Belum Dipilih</option>
                                            <option value="ontr"
                                                {{isset($datas->statusplat) && $datas->statusplat == 'ontr'? 'selected' : '' }}>
                                                On The Road</option>
                                            <option value="kredit"
                                                {{isset($datas->statusplat) && $datas->statusplat == 'offtr'? 'selected' : ''}}>
                                                Off The Road</option>
                                        </select>
                                        <small class="text-muted">Silahkan Masukkan Status Plat</small>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="grid-input-sm-2" class="col-md-3 control-label">Nama
                                        Plat</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="namaplat" name="namaplat"
                                            placeholder="Masukkan Nama Plat"
                                            value="{{ isset($datas->namaplat)? $datas->namaplat : '' }}">
                                        <small class="text-muted">Silahkan Masukkan Nama Plat</small>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="grid-input-sm-2" class="col-md-3 control-label">Warna
                                        TNKB</label>
                                    <div class="col-md-9">
                                        <select class="custom-select form-control" id="tnkb" name="tnkb">
                                            <option>Belum Dipilih</option>
                                            <option value="Hitam"
                                                {{ isset($datas->warnaplat) && $datas->warnaplat == 'Hitam'? 'selected' : '' }}>
                                                Hitam</option>
                                            <option value="Merah"
                                                {{ isset($datas->warnaplat) && $datas->warnaplat == 'Merah'? 'selected' : '' }}>
                                                Merah</option>
                                            <option value="Kuning"
                                                {{ isset($datas->warnaplat) && $datas->warnaplat == 'Kuning'? 'selected' : '' }}>
                                                Kuning</option>
                                            <option value="Hijau"
                                                {{ isset($datas->warnaplat) && $datas->warnaplat == 'Hijau'? 'selected' : '' }}>
                                                Hijau</option>
                                            <option value="Putih"
                                                {{ isset($datas->warnaplat) && $datas->warnaplat == 'Putih'? 'selected' : '' }}>
                                                Putih</option>
                                        </select>
                                        <small class="text-muted">Silahkan Masukkan Namaplat</small>
                                    </div>
                                </div>

                                <div class="form-group form-group-sm">
                                    <label for="grid-input-sm-2" class="col-md-3 control-label">KTP :</label>
                                    <div>
                                        <img src="/spk/{{$datas->no}}/thumbnail/KTP_{{$datas->no}}.png">
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label for="grid-input-sm-2" class="col-md-3 control-label">NPWP :</label>
                                        <div>
                                            <img src="/spk/{{$datas->no}}/thumbnail/NPWP_{{$datas->no}}.png">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                        <div class="modal fade" id="rangka{{str_replace('.', '', $datas->no)}}" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Identitas Kendaraan <br>
                                            {{ $datas->nama.' '.$datas->tipe.' '.$datas->warna.' '.$datas->tahun }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"
                                            action="admsales/{{$datas->nospk}}/insert/norangka" method="POST">
                                            <div class="form-group form-group-sm">
                                                <input type="hidden" name="nokendaraan"
                                                    value="{{$datas->detailkendaraanid}}">
                                                <label for="grid-input-sm-1" class="col-md-3 control-label">Nomor
                                                    Rangka</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_rangka"
                                                        name="no_rangka" placeholder="Masukkan Nomor Rangka Kendaraan"
                                                        value="{{ isset($datas->no_rangka)? $datas->no_rangka : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Rangka</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Nomor
                                                    Seri</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_seri" name="no_seri"
                                                        placeholder="Masukkan Nomor Seri Kendaraan"
                                                        value="{{ isset($datas->no_seri)? $datas->no_seri : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Seri</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Nomor
                                                    Mesin</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_mesin"
                                                        name="no_mesin" placeholder="Masukkan Nomor Mesin Kendaraan"
                                                        value="{{ isset($datas->no_mesin)? $datas->no_mesin : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Mesin</small>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="rangka{{str_replace('.', '', $datas->no)}}" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Identitas Kendaraan <br>
                                            {{ $datas->nama.' '.$datas->tipe.' '.$datas->warna.' '.$datas->tahun }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"
                                            action="admsales/{{$datas->nospk}}/insert/norangka" method="POST">
                                            <div class="form-group form-group-sm">
                                                <input type="hidden" name="nokendaraan"
                                                    value="{{$datas->detailkendaraanid}}">
                                                <label for="grid-input-sm-1" class="col-md-3 control-label">Nomor
                                                    Rangka</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_rangka"
                                                        name="no_rangka" placeholder="Masukkan Nomor Rangka Kendaraan"
                                                        value="{{ isset($datas->no_rangka)? $datas->no_rangka : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Rangka</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Nomor
                                                    Seri</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_seri" name="no_seri"
                                                        placeholder="Masukkan Nomor Seri Kendaraan"
                                                        value="{{ isset($datas->no_seri)? $datas->no_seri : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Seri</small>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-sm">
                                                <label for="grid-input-sm-2" class="col-md-3 control-label">Nomor
                                                    Mesin</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="no_mesin"
                                                        name="no_mesin" placeholder="Masukkan Nomor Mesin Kendaraan"
                                                        value="{{ isset($datas->no_mesin)? $datas->no_mesin : '' }}">
                                                    <small class="text-muted">Silahkan Masukkan Nomor Mesin</small>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn" data-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection