<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin"
        rel="stylesheet" type="text/css">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">

    <!-- Core stylesheets -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">

    <!-- Theme -->
    <link href="/css/themes/asphalt.min.css" rel="stylesheet" type="text/css">

    <!-- Pace.js -->
    <script src="/pace/pace.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row" style="heights: 1px; ">
            <div class="col-md-2 col-xs-2">
                <br>
                <img align="middle" src="/picture/sbm.png" width="80" height="80">
                </img>
            </div>
            <div class="col-md-7 col-xs-7">
                <br>
                <font size="1">MITSUBISHI MOTORS and MITSUBISHI FUSO TRUCK & BUS authorized dealer</font>
                <br>
                <b>
                    <font size="5">PT. SUMBER BERLIAN MOTORS</font>
                </b>
            </div>
            <div class="col-md-3 col-xs-3">
                <br>
                <div style="border:1px solid black; text-align:center">
                    <strong>
                        <font size="3" align="center">Lembar 1 Untuk Sales</font>
                    </strong>
                </div>
                <b>
                    <font size="1">SURAT PESANAN KENDARAAN</font>
                </b>
                <br><b>
                    <font size="1">No : {{$id}}</font>
                </b>
                <br><b>
                    <font size="1">Tgl : {{$dataspk->tanggal}}</font>
                </b>
            </div>
        </div>

        <table align="left" border="0">
            <tbody>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font>Head Office</font>
                    </td>
                    <td style="font-size: 9px;">
                        : Jl. A. Yani Km. 10,3 No.1 RT.5 Kertak Hanyar - Kab.Banjar Tlp.
                        0511-4281699, Fax
                        0511-4281664/65<br>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Banjarmasin</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. A. Yani Km. 5,7 No.51 - Banjarmasin, Tlp. 0511-3252217/3252218 Fax.
                            0511-3255657</font><br>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Martapura</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. A. Yani Km. 38 No. 49 Sel. Paring Martapura - Kab. Banjar Tlp.
                            0511-4720551/4720552, Fax.
                            0511-4720553<br></font>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Binuang</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. A. Yani Km. 88,5 Kel. karangan Putih Kec. Binuang - Kab. Tapin Tlp.0822
                            5040
                            5945<br></font>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Tanjung</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. A. Yani RT.04 Mabural - Kec. Murung Pundak, Tanjung Tlp 0852-2027398,
                            Fax.
                            0526-2027399<br>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Sungai Danau</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. A. Yani Km. 161 Kel. Sungai Danau Kec. Satul - Kab. Tanah Bumbu Tlp.
                            0822 5537
                            9327/0822 5537 9328<br>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td width="22%" style="font-size: 9px;">
                        <font size="1">Cab. Batulicin</font>
                    </td>
                    <td style="font-size: 9px;">
                        <font size="1">: Jl. Raya Batulicin No.127 RT.01 Kel. Kamp.Baru Kec. Simp.4 Kab.Tanah Bumbu
                            Tlp.
                            0518-708984/70194,
                            Fax. 0518-70632<br>
                        </font>
                    </td>
                </tr>
            </tbody>
        </table>


        <table border="1" align="left">
            <tr>
                <td style="font-size: 10px;">
                    <table style="table-layout:fixed; width:400px">
                        <tr>
                            <td valign="top" style="width:40%"><b>Nama Pemesan</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->namacust) ? $dataspk->namacust : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Alamat KTP<b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->alamatktp) ? $dataspk->alamatktp : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Pembelian</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                @if (isset($dataspk->keperluan) && $dataspk->keperluan == 'ku')
                                Kantor / Usaha
                                @elseif(isset($dataspk->keperluan) && $dataspk->keperluan == 'personal')
                                Pribadi
                                @elseif(isset($dataspk->keperluan) && $dataspk->keperluan == 'dll')
                                Lain - Lain
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>No. KTP</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->noktp) ? $dataspk->noktp : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>No. Telp/HP</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->notelp) ? $dataspk->notelp : ''}}</td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 10px;">
                    <table style="table-layout:fixed; width:400px">
                        <tr>
                            <td valign="top" style="width:40%"><b>Faktur STNK a/n.</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->anfakturstnk) ? $dataspk->anfakturstnk : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Alamat Domisili</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->alamatstnk) ? $dataspk->alamatstnk : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Faktur Pajak a/n.</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->anpajak) ? $dataspk->anpajak : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>NPWP / NIK</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->npwpnik) ? $dataspk->npwpnik : ''}}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Kontrak a/n.</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{isset($dataspk->ankontrak) ? $dataspk->ankontrak : ''}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-size: 11px;">
                    <table style="table-layout:fixed; width:400px">
                        <tr>
                            <td valign="top" style="width:40%"><b>Tipe / Bentuk</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{ $dataspk->nama.' / '.$dataspk->tipe }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Tahun / Warna</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{ $dataspk->tahun.' / '.$dataspk->warna }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Karoseri</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word">
                                {{ isset($dataspk->namakaroseri) ? $dataspk->namakaroseri : '' }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Lain-Lain</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>
                            <td valign="top" style="overflow: hidden; word-wrap:break-word"></td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 10px;">
                    <table style="table-layout:fixed; width:400px">
                        <tr>
                            <td style="width:40%">
                                @if ($dataspk->statusplat == 'ontr')
                                <img src="/icon/check.png" width="30" height="30">
                                @elseif ($dataspk->statusplat == 'offtr')
                                <img src="/icon/uncheck.png" width="30" height="30">
                                @endif
                                <b>On The Road</b></td>
                            <td style="width:1%" align="left"></td>
                            <td style="width:25%" align="left"><b>Plat:</b>
                                {{isset($dataspk->namaplat) ? $dataspk->namaplat : ''}}</td>
                            <td style="width:30%">
                                @if ($dataspk->statusplat == 'offtr')
                                <img src="/icon/check.png" width="30" height="30">
                                @elseif ($dataspk->statusplat == 'ontr')
                                <img src="/icon/uncheck.png" width="30" height="30">
                                @endif
                                <b>Off The Road</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:40%"><b>Pembayaran</b></td>
                            <td valign="top" style="width:1%"><b>:</b></td>

                            <td valign="top" style="width:39%">@if ($dataspk->jenispembayaran == 'tunai')Tunai /
                                <strike>Kredit</strike>@elseif($dataspk->jenispembayaran == 'kredit')
                                <strike>Tunai</strike> / Kredit @else Tunai / Kredit @endif </td>

                            <td valign="top" style="width:20%">Via :</td>
                        </tr>
                        <tr>
                            <td style="width:10%"><b>Paket Kredit</b></td>
                            <td style="width:1%"><b>:</b></td>
                            <td valign="top" style="width:30%">@if ($dataspk->jeniskredit == 'normal')<img
                                    src="/icon/check.png" width="30" height="30">Normal</td>
                            @else
                            <img src="/icon/uncheck.png" width="30" height="30">Normal
                </td>
                @endif

                <td valign="top" style="width:30%">@if ($dataspk->jeniskredit == 'subsidi')<img src="/icon/check.png"
                        width="30" height="30">Subsidi</td>@else <img src="/icon/uncheck.png" width="30"
                    height="30">Subsidi</td> @endif
            </tr>
            <tr>
                <td valign="top" style="width:40%"><b>Warna TNKB</b></td>
                <td valign="top" style="width:1%"><b>:</b></td>
                <td valign="top" style="overflow: hidden; word-wrap:break-word">{{$dataspk->warnaplat}}</td>
            </tr>
        </table>
        </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 11px;">
                <table style="table-layout:fixed; width:680px">
                    <tr>
                        <td style="width:10%"><b>No. Seri</b></td>
                        <td style="width:16%"><b>: {{isset($dataspk->no_seri) ? $dataspk->no_seri : ''}}</b></td>
                        <td style="width:10%"><b>No. Rangka</b></td>
                        <td style="width:16%"><b>: </b>{{isset($dataspk->no_rangka) ? $dataspk->no_rangka : ''}}
                        </td>
                        <td style="width:10%"><b>No. Mesin</b></td>
                        <td style="width:16%"><b>: </b>{{isset($dataspk->no_mesin) ? $dataspk->no_mesin : ''}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="font-size: 10px;table-layout:fixed; width:400px">
                    <tr>
                        <td valign="top" style="width:40%"><b>Harga Per Unit</b></td>
                        <td valign="top" style="width:1%"><b>:</b></td>
                        <td valign="top" style="overflow: hidden; word-wrap:break-word">Rp.
                            {{number_format($dataspk->harga)}}</td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:40%"><b>Karoseri</b></td>
                        <td valign="top" style="width:1%"><b>:</b></td>
                        <td valign="top" style="overflow: hidden; word-wrap:break-word">Rp.
                            {{isset($dataspk->biaya_karoseri) ? $dataspk->biaya_karoseri : ''}}</td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:40%"><b>Diskon</b></td>
                        <td valign="top" style="width:1%"><b>:</b></td>
                        <td valign="top" style="overflow: hidden; word-wrap:break-word">Rp.
                            {{isset($dataspk->diskon) ? number_format($dataspk->diskon) : ''}}</td>
                    </tr>
                </table>
            </td>
            <td style="font-size: 10px;">
                <table style="table-layout:fixed; width:400px" border="1">
                    <tr>
                        <td valign="top" align="center" colspan="4"><b>Deposit :</b></td>
                    </tr>
                    <tr>
                        <td valign="top" align="center"><b>Tgl.</b></td>
                        <td valign="top" align="center"><b>Via</b></td>
                        <td valign="top" align="center"><b>Nilai</b></td>
                        <td valign="top" align="center"><b>Atas Nama</b></td>
                    </tr>
                    @foreach ($datadeposit as $deposit)
                        <tr>
                        <td style="height:15px" valign="top" align="center">{{$deposit->tanggal}}</td>
                            <td style="height:15px" valign="top" align="center">{{$deposit->via}}</td>
                            <td style="height:15px" valign="top" align="center">{{$deposit->nilai}}</td>
                            <td style="height:15px" valign="top" align="center">{{$deposit->atasnama}}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size: 11px;">
                <table style="table-layout:fixed; width:400px">
                    <tr>
                        <td valign="top" style="width:40%"><b>Total Harga</b></td>
                        <td valign="top" style="width:1%"><b>:</b></td>
                        <td valign="top" style="overflow: hidden; word-wrap:break-word">Rp.
                            {{number_format($dataspk->harga + $dataspk->biaya_karoseri - $dataspk->diskon)}}</td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:40%"><b>Uang Muka</b></td>
                        <td valign="top" style="width:1%"><b>:</b></td>
                        <td valign="top" style="overflow: hidden; word-wrap:break-word">Rp.
                            {{number_format($dataspk->dp)}}</td>
                    </tr>
                </table>
            </td>
            <td rowspan="2">
                <table style="table-layout:fixed; width:400px">
                    <tr>
                        <td align="center" valign="top" style="width:40%">Catatan : <br> </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="overflow: hidden; word-wrap:break-word"> <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="table-layout:fixed; width:400px">
                    <tr>
                        <td valign="top" style="font-size: 11px; width:40%"><b>Tersisa</b></td>
                        <td valign="top" style="font-size: 11px; width:1%"><b>:</b></td>
                        <td valign="top" style="font-size: 11px; overflow: hidden; word-wrap:break-word">Rp.
                            {{number_format($dataspk->harga + $dataspk->biaya_karoseri - $dataspk->diskon - $dataspk->dp)}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="table-layout:fixed; width:400px">
                    <tr>
                        <td valign="top"><i>SYARAT & KENTENTUAN PESANAN :</i></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="400px" height="100px">
                <table style="table-layout:fixed; border=" 1">
                    <tr>
                        <td valign="top">
                            <ol valign="top" style="font-size: 10px; padding-left:1; list-style-position:outside;">
                                <li>Harga yang tercantum pada Surat Pesanan ini tidak mengikat, tergantung pada saat
                                    dilakukan penyerahan kendaraan
                                    dan Surat Pesanan ini bukan merupakan bukti pembayaran.</li>
                                <li>Surat Pemesanan ini diakui sah apabila :
                                    <ol type="a">
                                        <li>Pemesan telah membayar deposit dengan cara setor Tunai atau Transfer via
                                            Bank ke rekening PT. Sumber
                                            Berlian Motors dengan mendapatkan Tanda Terima / Kwitansi resmi dari PT.
                                            Sumber Berlian Motors.</li>
                                        <li>Telah ditanda-tangani oleh Pemesan, Penjual, Kacab, dan Koord. Penjualan
                                            / Manager.</li>
                                    </ol>
                                </li>
                                <li>Setiap Pembayaran dianggap sah apabila ada Tanda Terima/Kwitansi Resmi yang
                                    dikeluarkan oleh PT. Sumber Berlian
                                    Motors <font color="red">(Bukan Tanda Terima/Kwitansi yang dipakai oleh Umum).
                                    </font>
                                </li>
                                <li>Pembatalan Secara sepihak atas Pesanan ini oleh Pemesan, maka akan dikenakan
                                    sanksi secara Administratif atau
                                    Denda.</li>
                                <li>
                                    Tanda Terima/Kwitansi Resmi + Cap Resmi wajib dimnta sebagai bukti pemesanan
                                    yang sah.
                                </li>
                            </ol>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="400px" height="100">
                <table style="table-layout:fixed;">
                    <tr>
                        <td valign="top">
                            <ol style="font-size: 10px; margin: 0;" start="6">
                                <li>
                                    Deposit berlaku maksimal untuk jangka waktu 3 (tiga) bulan dan jika lewat, maka
                                    Pesanan secara Otomatis dianggap
                                    “Batal”.
                                </li>
                                <li>
                                    Harga dalam Surat Pesanan ini (jika On The Road) khusus untuk plat “DA”.
                                </li>
                                <li>
                                    Serah terima kendaraan di Showroom PT. Sumber Berlian Motors dan bilamana unit
                                    diantar ketempat Pembeli, maka
                                    akan dikenakan biaya dan segala resiko sepenuhnya menjadi tanggung jawab
                                    Pembeli.
                                </li>
                                <li>
                                    Perusahaan tidak bertanggung jawab terhadap setiap perjanjian yang isinya diluar
                                    dari Surat Pesanan Kendaraan
                                    ini.
                                </li>
                                <li>
                                    Setiap ada kenaikan Bea Balik Nama (BBN) sesuai dengan peraturan dari
                                    Pemerintah, maka kenaikan tersebut akan
                                    menjadi tanggungan Pembeli.
                                </li>
                                <li>
                                    Pembeli menyatakan bahwa fotocopy data pribadi atau perusahaan yang diserahkan
                                    sebagai syarat untuk lampiran
                                    dari Surat Pesanan ini adalah sesuai dengan aslinya, dan bilamana ternyata tidak
                                    sesuai, maka segala resiko yang
                                    akan timbul dikemudian hari sepenuhnya menjadi tanggung jawab dari Pembeli.
                                </li>
                                <li>
                                    Bilamana atas kendaraan yang dibelinya terkena ketentuan Pajak Progresif, maka
                                    Pembeli menyatakan bersedia
                                    untuk membayarnya.
                                </li>
                            </ol>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="table-layout:fixed;">
                    <tr>
                        <td valign="top" style="font-size: 10px;">
                            <font color="red">Semua transaksi pembayaran harus disetorkan Tunai pada Kasir Resmi
                                atau melalui transfer ke rekening PT Sumber Berlian
                                Motors, pada : Bank MANDIRI Cab. Lambung Mangkurat – Banjarmasin : 031.000000.3056
                                atau Bank BRI Cab. A. Yani –
                                Banjarmasin : 0623-01-000277-30-8 atau Bank BCA Cab. Lambung Mangkurat – Banjarmasin
                                : 051.305017.5</font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="table-layout:fixed;">
                    <tr>
                        <td valign="top" style="font-size: 10px;">
                            <b>DISCLAIMER : </b>
                            <p> Menanda-tangani dokumen ini, maka Anda menyatakan setuju bahwa PT Sumber Berlian
                                Motors selaku
                                Dealer Resmi produk Mitsubishi Motors ( PT Mitsubishi Motors Krama Yudha Sales
                                Indonesia) dan produk Mitsubishi Fuso
                                Truck & Bus (PT Krama Yudha Tiga Berlian Motors) beserta afiliasi maupun agen
                                resminya dapat menggunakan data dan
                                informasi yang ada dalam dokumen ini untuk menghubungi anda dengan tujuan dan
                                kepentingan, antara lain :</p>
                            <ol type="a">
                                <li>Proses transaksi & registrasi kendaraan serta layaan purna jual (After Sales
                                    Service) kendaraan Mitsubishi.</li>
                                <li>Survey kepuasaan pelanggan (Customer Satisfactions atas produk maupun layanan
                                    dari Dealer kendaraan Mitsubishi. </li>
                                <li>Informasi kegiatan & promosi produk maupun layanan terhadap kendaraan
                                    Mitsubishi.</li>
                            </ol>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="table-layout:fixed;">
                    <tr>
                        <td valign="top" style="font-size: 10px;">
                            <font color="red">Dengan menanda-tangani Surat Pesanan Kendaraan ini, maka
                                Pemesan/Pembeli dianggap setuju
                                dengan semua ketentuan dan
                                persyaratan yang tercantum dalam Surat Pesanan ini.</font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="table-layout:fixed;">
                    <tr>
                        <td valign="top" align="center" style="width:300px">
                        Pemesan :<br><img src="/spk/{{$id}}/signature_{{$id}}.png"
                                width="188" height="75"><br>{{$dataspk->namacust}}
                        </td>
                        <td valign="top" align="center" style="width:300px">
                            Penjual :<img src="/signature/MM.20.0001/signature_6371022605990011_MM.20.0001.png"
                                width="188" height="75"><br>
                            {{$dataspk->nama_user}}
                        </td>
                        <td valign="top" align="center" style="width:300px">
                            Kepala Cabang :
                            <img src="/signature/MM.20.0001/signature_6371022605990011_MM.20.0001.png" width="188"
                                height="75"><br>Nama Jelas
                        </td>
                        <td valign="top" align="center" style="width:300px">
                            Koord. Penjualan / Manager : <img
                                src="spk/MM.20.0028/signature/MM.20.0028/signature_6371029381928387_MM.20.0028.png"
                                width="188" height="75"> <br>Nama Jelas
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
        </table>
    </div>
    </div>
</body>

<!-- Load jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Core scripts -->
<script src="/js/bootstrap.min.js"></script>

<!-- Your scripts -->
<script src="/js/app.js"></script>

</html>