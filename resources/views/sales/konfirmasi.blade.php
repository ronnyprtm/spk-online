@extends('sales.layout')
@section('heading')
<h2>Konfirmasi Pengisian Data</h2>
@endsection
@section('content')
<style>
    #sheet-container {
        width: 250px;
        height: 100px;
        border: 1px solid black;
    }
</style>
<form class="container-fluid form-horizontal" name="myform" action="insert" method="POST" enctype="multipart/form-data">
    
    @csrf
    <!-- {{ csrf_field() }} -->
    @php
    if ($id != 'input') {
    echo ('<h2>No. SPK =' . $id  .'</h2>');
            }
            @endphp
            <div class="panel">
                <div class="panel-title">
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Customer
                            :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-2" id="namacust" name="namacust"
                                    value="{{ $namacust }}" readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Alamat KTP :</label>
                        <div class="col-md-9">
                            <strong><textarea class="form-control" name="alamatktp"
                                    placeholder="Masukkan Alamat Domisili" rows="2"
                                    readonly>{{$alamatktp}}</textarea></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Tujuan Pembelian :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-2" id="tujuanbeli" name="tujuanbeli"
                                    value="{{ $tujuanbeli }}" readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">No. KTP :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-2 " id="nomorktp" name="nomorktp"
                                    value="{{ $nomorktp }}" readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">No. Telp/HP :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-3" id="telpcust" name="telpcust"
                                    value="{{ $telpcust }}" readonly></strong>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">

                <div class="panel-title">
                </div>

                <div class="panel-body">
                    
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Faktur STNK a/n.:</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-5" id="namastnk" name="namastnk"
                                    value="{{ $namastnk }}" readonly></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Alamat Domisili :</label>
                        <div class="col-md-9">
                            <strong><textarea class="form-control" name="alamat" placeholder="Masukkan Alamat Domisili"
                                    rows="2" readonly>{{$alamatstnk}}</textarea></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Faktur Pajak a/n.:</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-5" id="namafaktur" name="namafaktur"
                                    value="{{ $namafaktur }}" readonly></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">NPWP / NIK :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-5" id="npwpnik" name="npwpnik"
                                    value="{{ $npwpnik }}" readonly></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Kontrak a/n. :</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-3" id="ankontrak" name="ankontrak"
                                    value="{{ $ankontrak }}" readonly></strong>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel col-md-6">
                <div class="panel-title">
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Tipe / Bentuk:</label>
                        <div class="col-md-9">
                            <input type="hidden" id="idkendaraan" name="idkendaraan" value="{{$kendaraan}}">
                            <strong><input type="text" class="form-control col-md-5" id="tipebentuk" name="tipebentuk"
                                    value="{{$tipe->nama}} / {{$tipe->tipe}}" readonly></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Tahun / Warna:</label>
                        <div class="col-md-9">
                            <strong><input type="text" class="form-control col-md-5" id="warnatahun" name="warnatahun"
                                    value="{{$tipe->tahun}} / {{$tipe->warna}}" readonly></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Karoseri :</label>
                        <div class="col-md-9">
                            <input type="hidden" id="idkaroseri" name="idkaroseri" value="{{$idkaroseri}}">
                            <strong><input type="text" class="form-control col-md-5" id="karoseri" name="karoseri"
                                    value="@if($tipe->bagian == "MFTBC"){{ $karoseri->namakaroseri }}@endif" 
                                    readonly></strong>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel col-md-6">
                <div class="panel-title">
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox col-md-4">
                                <label>
                                    <input type="checkbox" id="status" name="status" readonly value="ontr"
                                        @php if ($status=="ontr" ) { echo 'checked' ; } @endphp>On The Road</label>
                            </div>
                            <div class="checkbox col-md-4">
                                <strong><input type="text" class="form-control col-md-5" onclick="return false" id="ontrplat" name="ontrplat"
                                        value="{{$ontr}}" readonly></strong>
                            </div>
                            <div class="checkbox col-md-4">
                                <label>
                                    <input type="checkbox" value="offtr" onclick="return false" @php if ($status=="offtr" )
                                        { echo 'checked' ; } @endphp readonly>Off The Road
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Pembayaran:</label>
                        <div class="col-md-3">
                            <strong><input type="text" class="form-control col-md-3" id="jenispay" name="jenispay"
                                    value="{{$jenispay}}" readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Kredit Via:</label>
                        <div class="col-md-6">
                            <input type="hidden" class="form-control col-md-6" id="idfinance" name="idfinance"
                                value="{{$kvia}}" readonly>
                            <strong><input type="text" class="form-control col-md-6" id="krvia" name="krvia" value="{{isset($datafinance->nama_finance) ? $datafinance->nama_finance : ''}}"
                                    readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Jenis Kredit:</label>
                        <div class="col-md-6">
                            <strong><input type="text" class="form-control col-md-6" id="jekredit" name="jekredit"
                                    value="{{$jkredit}}" readonly></strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="loading-input-8" class="col-md-3 control-label">Warna TNKB</label>
                        <div class="col-md-3">
                            <strong><input type="text" class="form-control col-md-3" id="wtnkb" name="wtnkb"
                                    value="{{$warnatnkb}}" readonly></strong>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <div class="panel">
                <div class="panel-title">Pembayaran</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" name="lvia" for="success-input-1">Harga Per-Unit</label>
                            <input type="text" class="form-control" id="hargaunit" name="hargaunit" placeholder=""
                                value="{{$tipe->harga}}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" name="lvia" for="success-input-1">Diskon</label>
                            <input type="text" onkeyup="calculate()" class="form-control" id="diskon" name="diskon"
                                placeholder="Masukan Diskon" value="{{isset($data->diskon) ? $data->diskon : ''}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" name="lvia" for="success-input-1">Biaya Karoseri</label>
                            <input type="text" class="form-control" id="biayakaroseri" name="biayakaroseri"
                                placeholder="" value="{{isset($data->biaya_karoseri) ? $data->biaya_karoseri : ''}}" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label" name="lvia" for="success-input-1">Total Harga</label>
                            <input type="text" class="form-control" id="tharga" name="tharga" placeholder="" style="text-align:left;" 
                            value="@if(isset($data->diskon) && isset($data->id_karoseri)){{$tipe->harga + $data->biaya_karoseri - $data->diskon}}
                                    @elseif(isset($data->diskon)){{$tipe->harga - $data->diskon}}
                                    @elseif(isset($data->id_karoseri)){{$tipe->harga + $data->biaya_karoseri}}
                                    @endif" readonly>
                        </div>
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label" name="lvia" for="success-input-1">Uang Muka</label>
                            <input type="text" class="form-control" onkeyup="hitungdp()" id="dp" name="dp"
                                placeholder="" value="{{isset($data->dp) ? $data->dp : ''}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-6">
                            <label class="control-label" name="lvia" for="success-input-1">Sisa Pembayaran</label>
                            <input type="text" class="form-control" id="sisa" name="sisa" placeholder="" readonly value="@php 
                            echo $tipe->harga - $data->diskon + $data->biaya_karoseri - $data->dp;
                            @endphp">
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-title">Lampiran</div>
                <div class="panel-body">
                    <div class="form-group col-md-6">
                        <label class="control-label" for="success-input-1">Upload KTP</label>
                         <input type="file" name="filektp" id="filektp" accept="image*, .pdf">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label" for="success-input-1">Upload NPWP</label>
                        <input type="file" name="filenpwp" id="filenpwp" accept="image/*, .pdf">
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-title">Metode Pembayaran</div>
                <div class="panel-body">
                    <div class="row">
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                        <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder="" value="{{$datadeposit[0]->tanggal}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Via</label>
                        <input type="text" class="form-control" id="viapay" name="viapay[]"
                            placeholder="Masukkan Via Pembayaran" value="{{$datadeposit[0]->via}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                        <input type="text" class="form-control" id="nilaipay" name="nilaipay[]"
                            placeholder="Masukkan Nilai Pembayaran" value="{{$datadeposit[0]->nilai}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                        <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama" value="{{$datadeposit[0]->atasnama}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="success-input-1">Upload Bukti</label>
                        <input type="file" name="filebukti[]" id="filebukti" accept="image/*, .pdf">
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                        <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder="" value="{{$datadeposit[1]->tanggal}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Via</label>
                        <input type="text" class="form-control" id="viapay" name="viapay[]"
                            placeholder="Masukkan Via Pembayaran" value="{{$datadeposit[1]->via}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                        <input type="text" class="form-control" id="nilaipay" name="nilaipay[]"
                            placeholder="Masukkan Nilai Pembayaran" value="{{$datadeposit[1]->nilai}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                        <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama" value="{{$datadeposit[1]->atasnama}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="success-input-1">Upload Bukti</label>
                        <input type="file" name="filebukti[]" id="filebukti" accept="image/*, .pdf">
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                        <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder="" value="{{$datadeposit[2]->tanggal}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Via</label>
                        <input type="text" class="form-control" id="viapay" name="viapay[]"
                            placeholder="Masukkan Via Pembayaran" value="{{$datadeposit[2]->via}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                        <input type="text" class="form-control" id="nilaipay" name="nilaipay[]"
                            placeholder="Masukkan Nilai Pembayaran" value="{{$datadeposit[2]->nilai}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                        <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama" value="{{$datadeposit[2]->atasnama}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="success-input-1">Upload Bukti</label>
                        <input type="file" name="filebukti[]" id="filebukti" accept="image/*, .pdf">
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Tanggal</label>
                        <input type="date" class="form-control" id="tanggalpay" name="tanggalpay[]" placeholder="" value="{{$datadeposit[3]->tanggal}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Via</label>
                        <input type="text" class="form-control" id="viapay" name="viapay[]"
                            placeholder="Masukkan Via Pembayaran" value="{{$datadeposit[3]->via}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Nilai</label>
                        <input type="text" class="form-control" id="nilaipay" name="nilaipay[]"
                            placeholder="Masukkan Nilai Pembayaran" value="{{$datadeposit[3]->nilai}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label class="control-label" name="lvia" for="success-input-1">Atas Nama</label>
                        <input type="text" class="form-control" id="anpay" name="anpay[]" placeholder="Masukkan Nama" value="{{$datadeposit[3]->atasnama}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="success-input-1">Upload Bukti</label>
                        <input type="file" name="filebukti[]" id="filebukti" accept="image/*, .pdf">
                    </div>
                    </div>
                    

                    <div class="form-group col-md-12">
                        <textarea class="form-control" name="alamatktp" placeholder="Masukkan Catatan"
                            rows="2"></textarea>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-title">Syarat & Ketentuan Pesanan</div>
                <div class="panel-body">
                    <div class="panel col-md-6">
                        <div class="panel-body">
                            <p>
                                1. Harga yang tercantum pada Surat Pesanan ini tidak mengikat, tergantung pada saat
                                dilakukan penyerahan kendaraan dan
                                Surat Pesanan ini bukan merupakan bukti pembayaran.<br>
                                2. Surat Pemesanan ini diakui sah apabila : <br>
                                a. Pemesan telah membayar deposit dengan cara setor Tunai atau Transfer via Bank ke
                                rekening PT. Sumber Berlian Motros
                                dengan mendapatkan Tanda Terima / Kwitansi resmi dari PT. Sumber Berlain Motors. <br>
                                b. Telah ditanda-tangani oleh Pemesan, Penjual, Kacab, dan Koord. Penjualan / Manager.
                                <br>
                                3. Setiap Pembayaran dianggap sah apabila ada Tanda Terima/Kwitansi Resmi yang
                                dikeluarkan oleh PT. Sumber Berlian
                                Motors <font color="red">(BUKAN Tanda Terima/Kwitansi yang dipakai oleh Umum).</font>
                                <br>
                                4. Pembatalan Secara sepihak atas Pesanan ini oleh Pemesan, maka akan dikenakan sanksi
                                secara Administratif atau Denda.
                                5. Tanda Terima/Kwitansi Resmi + Cap Resmi wajib dimnta sebagai bukti pemesanan yang
                                sah. <br>
                                6. Deposit berlaku maksimal untuk jangka waktu 3 (tiga) bulan dan jika lewat, maka
                                Pesanan secara Otomatis dianggap
                                “Batal”.</p>
                        </div>
                    </div>
                    <div class="panel col-md-6">
                        <div class="panel-body">
                            <p>
                                7. Harga dalam Surat Pesanan ini (jika On The Road) khusus untuk plat “DA”. <br>
                                8. Serah terima kendaraan di Showroom PT. Sumber Berlian Motors dan bilamana unit
                                diantar ketempat Pembeli, maka akan
                                dikenakan biaya dan segala resiko sepenuhnya menjadi tanggung jawab Pembeli.<br>
                                9. Perusahaan tidak bertanggung jawab terhadap setiap perjanjian yang isinya diluar dari
                                Surat Pesanan Kendaraan ini.
                                10. Setiap ada kenaikan Bea Balik Nama (BBN) sesuai dengan peraturan dari Pemerintah,
                                maka kenaikan tersebut akan
                                menjadi tanggungan Pembeli.<br>
                                11. Pembeli menyatakan bahwa fotocopy data pribadi atau perusahaan yang diserahkan
                                sebagai syarat untuk lampiran dari
                                Surat Pesanan ini adalah sesuai dengan aslinya, dan bilamana ternyata tidak sesuai, maka
                                segala resiko yang akan timbul
                                dikemudian hari sepenuhnya menjadi tanggung jawab dari Pembeli.<br>
                                12. Bilamana atas kendaraan yang dibelinya terkena ketentuan Pajak Progresif, maka
                                Pembeli menyatakan bersedia uuntuk
                                membayarnya.</p><br>
                        </div>
                    </div>
                    <div class="panel col-md-12">
                        <div class="panel-body">
                            <p>
                                <font color="red">
                                    Semua transaksi pembayaran harus disetorkan Tunai pada Kasir Resmi atau melalui
                                    transfer ke rekening PT Sumber Berlian
                                    Motors, pada :
                                    <br> Bank MANDIRI Cab. Lambung Mangkurat – Banjarmasin : 031.000000.3056 atau
                                    <br> Bank BRI Cab. A. Yani – Banjarmasin : 0623-01-000277-30-8 atau
                                    <br> Bank BCA Cab. Lambung Mangkurat – Banjarmasin : 051.305017.5.
                                </font>
                            </p>
                        </div>
                    </div>
                    <div class="panel col-md-12">
                        <div class="panel-body">
                            <p>
                                <strong> DISCLAIMER : </strong> Dengan menanda-tangani dokumen ini, maka Anda menyatakan
                                setuju bahwa PT Sumber
                                Berlian Motors selaku
                                Dealer Resmi produk Mitsubishi Motors ( PT Mitsubishi Motors Krama Yudha Sales
                                Indonesia) dan produk
                                Mitsubishi Fuso
                                Truck & Bus (PT Krama Yudha Tiga Berlian Motors) beserta afiliasi maupun agen resminya
                                dapat menggunakan
                                data dan
                                informasi yang ada dalam dokumen ini untuk menghubungi anda dengan tujuan dan
                                kepentingan, antara lain :<br>

                                a. Proses transaksi & registrasi kendaraan serta layaan purna jual (After Sales Service)
                                kendaraan
                                Mitsubishi.<br>
                                b. Survey kepuasaan pelanggan (Customer Satisfactions atas produk maupun layanan dari
                                Dealer kendaraan
                                Mitsubishi.<br>
                                c. Informasi kegiatan & promosi produk maupun layanan terhadap kendaraan Mitsubishi.<br>
                            </p>
                        </div>
                    </div>
                    <div class="panel col-md-12">
                        <div class="panel-body">
                            <p>
                                <font color="red">Dengan menanda-tangani Surat Pesanan Kendaraan ini, maka
                                    Pemesan/Pembeli dianggap setuju
                                    dengan semua ketentuan dan
                                    persyaratan yang tercantum dalam Surat Pesanan ini.</font>
                            </p>
                        </div>
                    </div>
                </div>

                <div style="width:800px; margin:0 auto;">
                    
                <div id="sheet-container">
                    <canvas id="sheet" width="250" height="100"></canvas>
                </div>
                <input type="button" class="btn btn-default btn-sm" id="saveSign" value="Kunci Tanda Tangan">
                <button class="btn btn-danger btn-sm" id="clearSignature">Reset Tanda Tangan</button>
                <!--Add signature here -->
                <input type="hidden" name="signatureurl" id="signatureurl" value="">
                <!--->
                </div>
                <br>
                    <div id="signature">
                    </div>
                <div>
                    <button type="submit" class="btn btn-success">Success</button>
                </div>
</form>


@endsection

@section('js')
<script>
    var canvas = new fabric.Canvas('sheet');
            canvas.isDrawingMode = true;
            canvas.freeDrawingBrush.width = 1;
            canvas.freeDrawingBrush.color = "#000000";
            $( '#saveSign' ).click( function( e ) {
                e.preventDefault();
                var canvas = document.getElementById("sheet");
                var dataUrl =  canvas.toDataURL("image/png"); //
                var saveButton = $(this).val();
                if(saveButton == "Kunci Tanda Tangan"){
                    //alert(dataUrl); check if canvas is empty
                    var blank = isCanvasBlank(canvas);
                    if(blank){
                        alert('Tanda Tangan Tidak Boleh Kosong');
                        return false;
                    }
                    //Pass signature to the form.
                    var signature = document.getElementById('signature');
                    document.getElementById('signatureurl').value = dataUrl;
                    $(this).val('Batalkan Mengunci Tanda Tangan'); //Update button text
                }else if(saveButton == "Batalkan Mengunci Tanda Tangan"){
                    var signature = document.getElementById('signature');
                    signature.innerHTML = '';
                    $(this).val("Add Signature");
                }
            });
            //Clear signature
           $('#clearSignature').click(function (e) {
               e.preventDefault();
               canvas.clear();
           });
           
           function focus(){
            document.getElementById('signature').focus();
           }

           //Check if canvass is empty
           function isCanvasBlank(canvas) {
               var blank = document.createElement('canvas');
               blank.width = canvas.width;
               blank.height = canvas.height;

               return canvas.toDataURL() == blank.toDataURL();
           }

           
</script>

<script text="type/javascript">
    var form = document.forms.myform,
    hargaunit = form.hargaunit,
    biayakaroseri = form.biayakaroseri,
    diskon = form.diskon,
    dp = form.dp,
    output = form.tharga,
    outputdp = form.sisa;
    
    window.calculate = function() {
    var h = parseInt(hargaunit.value) || 0,
    k = parseFloat(biayakaroseri.value) || 0,
    d = parseFloat(diskon.value) || 0;
    output.value = (h + k - d);
    };
</script>

<script text="type/javascript">
    var dp = form.dp,
    output = form.tharga,
    outputdp = form.sisa;
    window.hitungdp = function() {
        var dps = document.myform.dp.value || 0,
        output = document.myform.tharga.value;
        outputdp.value = (output - dps);
        }
</script>




@endsection