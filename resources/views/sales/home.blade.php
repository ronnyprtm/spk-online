@extends('sales.layout');
@section('heading')
<h2>Selamat Datang Di SPK Online</h2>
@endsection
@section('content')

    <div class="panel">
        <div class="panel-title">SPK Anda Bulan Ini</div>
        <div class="panel-body">
            <div class="table-info">
                <div class="table-header">
                    <div class="table-caption">
                        List SPK
                    </div>
                </div>
                <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No SPK</th>
                            <th>Kendaraan</th>
                            <th>Pembayaran</th>
                            <th>Nama <br>Customer</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $datas)
                        <tr>
                            <td>{{ $datas->nospk}}</td>
                            <td>{{$datas->nama.' '.$datas->tipe.' '.$datas->warna.' '.$datas->tahun}}</td>
                            <td>{{$datas->jenispembayaran}}</td>
                            <td>{{$datas->namacust}}</td>
                            <td>{{$datas->tanggal}}</td>
                            <td>
                                @php
                                if ($datas->admsalesverif == 1 && $datas->managerverif == 1 && $datas->branchverif == 1) {
                                echo 'Sudah Diverifikasi';
                                }else{
                                echo 'Belum Diverifikasi';
                                }
                                @endphp
                            </td>
                            <td>
                                @php
                                if ($datas->admsalesverif == 1 && $datas->managerverif == 1 && $datas->branchverif == 1) {
                                echo '<a href="/print"><button>Print</button></a>';
                                } else {
                                echo '<a href="/sales/form/'.$datas->nospk.'"><button class="btn btn-success">Edit</button></a>';
                                }
                                @endphp
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
@endsection