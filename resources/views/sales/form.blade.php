@extends('sales.layout');
@section('heading')
<h2>Form Surat Pemesanan Kendaraan</h2>
@endsection
@section('content')
{{-- {{dd($data->keperluan)}} --}}
<form class="container-fluid" action="{{$id}}/confirm" method="POST">
    @csrf
    <!-- {{ csrf_field() }} -->
    
    <div class="panel">
        <div class="panel-title">Data Pemesanan</div>
        <div class="panel-body">
            {{-- <div class="form-group col-md-3">
                <label class="control-label" for="success-input-1">No. SPK</label>
                <input type="text" class="form-control" name="nospk" disabled>
            </div> --}}
            <div class="form-group col-md-12">
                <label class="control-label" for="success-input-1">Nama Pemesan</label>
                <input type="text" class="form-control" name="namacust" placeholder="Masukan Nama Pemesan" 
                value="{{ isset($data->namacust)? $data->namacust : '' }}">
            </div>
            <div class="form-group col-md-12">
                <label class="control-label" for="success-input-1">Alamat Sesuai KTP</label>
                <textarea class="form-control" name="alamatktp" placeholder="Masukkan Alamat Sesuai KTP"
                    rows="3">{{ isset($data->alamatktp)? $data->alamatktp : '' }}</textarea>
            </div>
            <div class="form-group col-md-3">
                <label class="control-label" for="success-input-1">Tujuan Pembelian</label>
                <select class="custom-select form-control" id="tujuanbeli" name="tujuanbeli">
                    <option value="">Belum Dipilih</option>
                    <option value="ku" {{ isset($data->keperluan) && $data->keperluan == 'ku' ? 'selected' : '' }}>Kantor / Usaha</option>
                    <option value="personal" {{ isset($data->keperluan) && $data->keperluan == 'personal' ? 'selected' : '' }}>Pribadi</option>
                    <option value="dll" {{ isset($data->keperluan) && $data->keperluan == 'dll' ? 'selected' : '' }}>Lain-lain</option>
                </select>
            </div>
            <div class="form-group col-md-7">
                <label class="control-label" for="success-input-1">Nomor KTP</label>
                <input type="text" name="nomorktp" class="form-control" placeholder="Masukkan Nomor KTP" value="{{ isset($data->noktp)? $data->noktp : '' }}">
            </div>
            <div class="form-group col-md-5">
                <label class="control-label" for="success-input-1">Nomor Telepon / Handphone</label>
                <input type="text" name="telpcust" class="form-control"
                    placeholder="Masukkan Nomor Telepon / Handphone" value="{{ isset($data->notelp)? $data->notelp : '' }}">
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Faktur STNK Atas Nama</label>
                <input type="text" name="namastnk" class="form-control" placeholder="Masukkan Atas Nama Faktur STNK" 
                value="{{ isset($data->anfakturstnk)? $data->anfakturstnk : '' }}">
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Faktur Pajak Atas Nama</label>
                <input type="text" name="namafaktur" class="form-control" placeholder="Masukkan Atas Nama Pajak"
                value="{{ isset($data->anpajak)? $data->anpajak : '' }}">
            </div>
            <div class="form-group col-md-12">
                <label class="control-label" for="success-input-1">Alamat</label>
                <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat Domisili" rows="3">{{ isset($data->alamatstnk)? $data->alamatstnk : '' }}</textarea>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">NPWP / NIK</label>
                <input type="text" name="npwpnik" class="form-control" placeholder="Masukkan Nomor NPWP Atau NIK"
                value="{{ isset($data->npwpnik)? $data->npwpnik : '' }}">
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Kontrak Atas Nama</label>
                <input type="text" name="ankontrak" class="form-control" placeholder="Masukkan Atas Nama Kontrak" value="{{ isset($data->ankontrak)? $data->ankontrak : '' }}">
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-title">Detail Kendaraan</div>
        <div class="panel-body">
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Kendaraan</label>
                <input type="text" name="kendaraan" id="kendaraan" class="form-control"
                    placeholder="Masukkan Jenis Kendaraan" value="{{ isset($data->detailkendaraanid) ? $data->detailkendaraanid : '' }}">
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Karoseri</label>
                <select class="custom-select form-control" id="karoseri" name="karoseri">
                    <option value="">Belum Dipilih</option>
                    @foreach ($karoseri as $karo)
                    <option value="{{$karo->id}}" {{ isset($data->idkaroseri) && $data->idkaroseri == $karo->id? 'selected' : '' }}>{{$karo->text}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">No. Seri</label>
                <input type="text" class="form-control" name="noseri" placeholder="Masukkan No. Seri" 
                value="{{ isset($data->no_seri)? $data->no_seri : '' }}" readonly>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">No. Rangka</label>
                <input type="text" class="form-control" name="norangka" placeholder="Masukkan No. Rangka" 
                value="{{ isset($data->no_rangka)? $data->no_rangka : '' }}" readonly>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1" >No. Mesin</label>
                <input type="text" class="form-control" name="nomesin" placeholder="Masukkan No. Mesin"
                value="{{ isset($data->no_mesin)? $data->no_mesin : '' }}" readonly>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label for="grid-input-4" class="col-md-4 control-label">Status Plat</label>
                </div>
                <div class="col-md-2">
                    <select class="custom-select form-control" id="status" name="status">
                        <option>Belum Dipilih</option>
                        <option value="ontr" {{ isset($data->statusplat) && $data->statusplat == 'ontr'? 'selected' : '' }}>On The Road</option>
                        <option value="offtr" {{ isset($data->statusplat) && $data->statusplat == 'offtr'? 'selected' : '' }}>Off The Road</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6">
                <input type="text" class="form-control" id="ontr" name="ontr" placeholder="Masukkan Plat Kendaraan" 
                value="{{ isset($data->namaplat)? $data->namaplat : '' }}">
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1" id="lwarnatnkb" name="lwarnatnkb">Warna TNKB</label>
                <select class="custom-select form-control" id="warnatnkb" name="warnatnkb">
                    <option>Belum Dipilih</option>
                    <option value="Hitam" {{ isset($data->warnaplat) && $data->warnaplat == 'Hitam'? 'selected' : '' }}>Hitam</option>
                    <option value="Merah" {{ isset($data->warnaplat) && $data->warnaplat == 'Merah'? 'selected' : '' }}>Merah</option>
                    <option value="Kuning" {{ isset($data->warnaplat) && $data->warnaplat == 'Kuning'? 'selected' : '' }}>Kuning</option>
                    <option value="Hijau" {{ isset($data->warnaplat) && $data->warnaplat == 'Hijau'? 'selected' : '' }}>Hijau</option>
                    <option value="Putih" {{ isset($data->warnaplat) && $data->warnaplat == 'Putih'? 'selected' : '' }}>Putih</option>
                </select>
            </div>
        </div>
    </div>

    <div class="panel">
        <div class="panel-title">Metode Pembayaran</div>
        <div class="panel-body">
            <div class="form-group col-md-6">
                <label class="control-label" for="success-input-1">Jenis Pembayaran</label>
                <select class="custom-select form-control" id="jenispay" name="jenispay">
                    <option>Belum Dipilih</option>
                    <option value="tunai" {{ isset($data->jenispembayaran) && $data->jenispembayaran == 'tunai'? 'selected' : '' }}>Tunai</option>

                    <option value="kredit" {{ isset($data->jenispembayaran) && $data->jenispembayaran == 'kredit'? 'selected' : '' }}>Kredit</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label" name="lkvia" id="lkvia" for="success-input-1">Via</label>
                <select class="custom-select form-control" id="kvia" name="kvia">
                    <option value="">Belum Dipilih</option>
                    @foreach ($kredit as $kredit)
                    <option value="{{$kredit->id}}" {{ isset($data->idfinance) == $kredit->id? 'selected' : '' }}>{{$kredit->text}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label" name="tvia" id="tvia" for="success-input-1">Via</label>
                <input type="text" class="form-control" id="ftvia" name="ftvia" placeholder="Masukkan Via Pembayaran">
            </div>

            <div class="form-group col-md-6">
                <label class="control-label" name="tjkredit" id="tjkredit" for="success-input-1">Jenis
                    Kredit</label>
                <select class="custom-select form-control" id="jkredit" name="jkredit">
                    <option value="">Belum Dipilih</option>
                    <option value="normal" {{ isset($data->jeniskredit) == 'normal'? 'selected' : '' }}>Bunga Normal</option>
                    <option value="subsidi"
                        {{ isset($data->warnaplat) == 'subsidi'? 'selected' : '' }}>Bunga Subsidi</option>
                </select>
            </div>
        </div>
    </div> 

    <button type="submit" class="btn btn-success">Submit</button>
</form>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        var datam = <?= json_encode($vehicle); ?>;
    $("#kendaraan").select2({
    placeholder : "Pilih Kendaraan",
    data : datam,
    });
    });
</script>

<script type="text/javascript">
    $('#status').on('change',function(){
if($(this).val()==="ontr"){
$("#ontr").show(),
$("#warnatnkb").show()
$("#lwarnatnkb").show()
}
else{
$("#ontr").hide(),
$("#warnatnkb").hide(),
$("#lwarnatnkb").hide()
}
});


</script>

<script type="text/javascript">
    $('#jenispay').on('change',function(){
if($(this).val()==="kredit"){
$("#via").show(),
$("#jkredit").show(),
$("#tjkredit").show(),
$("#kvia").show(),
$("#lkvia").show(),
$("#tvia").hide(),
$("#ftvia").hide()
}
else if ($(this).val()==="tunai") {
    $("#via").hide(),
    $("#jkredit").hide(),
    $("#tjkredit").hide(),
    $("#kvia").hide(),
    $("#lkvia").hide(),
    $("#tvia").show(),
    $("#ftvia").show()
}
else{
$("#via").hide(),
$("#lkvia").hide(),
$("#kvia").hide(),
$("#jkredit").hide(),
$("#tjkredit").hide(),
$("#tvia").hide(),
$("#ftvia").hide()
}
});
</script>

@endsection